package com.rjapps.mpf.util;

public interface LicenceCallBack {
    void onItemClick(int position);

    void deleteItem(int position);
}
