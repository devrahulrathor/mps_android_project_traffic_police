package com.rjapps.mpf.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class MyApplication extends Application {
    private static MyApplication mInstance;
    public SharedPreferences preferences;
    public String prefName = "MPF";

    public MyApplication() {
        mInstance = this;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        mInstance = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }


    public void saveIsLogin(boolean flag) {
        preferences = getSharedPreferences(prefName, 0);
        Editor editor = preferences.edit();
        editor.putBoolean("IsLoggedIn", flag);
        editor.apply();
    }

    public void saveToken(String token, String refreshToken) {
        preferences = getSharedPreferences(prefName, 0);
        Editor editor = preferences.edit();
        editor.putString("token", token);
        editor.putString("refreshToken", refreshToken);
        editor.apply();
    }

    public boolean getIsLogin() {
        preferences = this.getSharedPreferences(prefName, 0);
        return preferences.getBoolean("IsLoggedIn", false);
    }

    public String getToken() {
        preferences = this.getSharedPreferences(prefName, 0);
        return preferences.getString("token", "");
    }

    public String getRefreshToken() {
        preferences = this.getSharedPreferences(prefName, 0);
        return preferences.getString("refreshToken", "");
    }


}
