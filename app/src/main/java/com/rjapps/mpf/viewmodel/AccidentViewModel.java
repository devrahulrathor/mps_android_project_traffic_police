package com.rjapps.mpf.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.rjapps.mpf.data.repo.accident.AccidentRepo;
import com.rjapps.mpf.data.repo.accident.callback.AccidentCallBack;

import java.io.File;
import java.util.List;

public class AccidentViewModel extends ViewModel {
    final AccidentRepo accidentRepo = new AccidentRepo();
    public MutableLiveData<String> reportedSuccess = new MutableLiveData<>();
    public MutableLiveData<String> reportFailed = new MutableLiveData<>();


    public void reportAnAccident(String token, List<File> images, List<String> licenseNumbers, String description, String latitude, String longitude) {
        accidentRepo.reportAccident(token, images, licenseNumbers, description, latitude, longitude, new AccidentCallBack() {
            @Override
            public void onSuccess(String success) {
                reportedSuccess.setValue(success);
            }

            @Override
            public void onError(String error) {
                reportFailed.setValue(error);
            }
        });

    }
}