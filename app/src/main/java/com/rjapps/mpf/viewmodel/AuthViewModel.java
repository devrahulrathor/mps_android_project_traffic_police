package com.rjapps.mpf.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.rjapps.mpf.data.repo.auth.AuthRepo;
import com.rjapps.mpf.data.repo.auth.callback.LogInCallBack;
import com.rjapps.mpf.data.repo.auth.model.LogIn;

import java.util.HashMap;

public class AuthViewModel extends ViewModel {
    final AuthRepo authRepo = new AuthRepo();
    public MutableLiveData<LogIn> loginSuccess = new MutableLiveData<>();
    public MutableLiveData<String> loginFailed = new MutableLiveData<>();


    public void logIn(HashMap<String, String> data) {
        authRepo.login(data, new LogInCallBack() {
            @Override
            public void onSuccess(LogIn logIn) {
                loginSuccess.setValue(logIn);
            }

            @Override
            public void onError(String error) {
                loginFailed.setValue(error);
            }
        });

    }
}
