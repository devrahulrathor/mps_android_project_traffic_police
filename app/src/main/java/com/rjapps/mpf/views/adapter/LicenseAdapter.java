package com.rjapps.mpf.views.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rjapps.mpf.R;
import com.rjapps.mpf.util.LicenceCallBack;

import java.util.List;

public class LicenseAdapter extends RecyclerView.Adapter<LicenseAdapter.ViewHolder> {
    private LicenceCallBack clickListener;
    private Context context;
    private List<String> list;

    public LicenseAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_license_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.licenseText.setText("" + list.get(position));
        holder.deleteImage.setOnClickListener(view -> clickListener.deleteItem(position));
    }

    public void setOnItemClickListener(LicenceCallBack clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView licenseText;
        ImageView deleteImage;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            licenseText = itemView.findViewById(R.id.license_test);
            deleteImage = itemView.findViewById(R.id.delete_licence);
        }
    }
}
