package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;
import com.rjapps.mpf.views.adapter.SpinnerAdapter;

public class IssueFineActivity extends AppCompatActivity {
    private Spinner idTypeSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_fine);
        init();
        setButtonAction();
        setSpinner();
    }

    void init() {
        idTypeSpinner = findViewById(R.id.id_type_spinner);
    }

    private void setSpinner() {
        SpinnerAdapter idTypeAdapter = new SpinnerAdapter(IssueFineActivity.this, android.R.layout.simple_list_item_1);
        idTypeAdapter.clear();
        idTypeAdapter.addAll(getResources().getStringArray(R.array.id_type));
        idTypeAdapter.add(getString(R.string.choose_your_id_type));
        idTypeSpinner.setAdapter(idTypeAdapter);
        idTypeSpinner.setSelection(idTypeAdapter.getCount());
        idTypeSpinnerColor();
    }

    private void idTypeSpinnerColor() {
        idTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String group = idTypeSpinner.getSelectedItem().toString();
//                if ("Choose your ID Type".equals(group)) {
//                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_grey));
//                } else {
//                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setButtonAction() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());
        findViewById(R.id.search_id).setOnClickListener(view -> {
            Intent intent = new Intent(IssueFineActivity.this, LicenseDetailActivity.class);
            intent.putExtra("type", "issue_fine");
            startActivity(intent);
        });
    }
}