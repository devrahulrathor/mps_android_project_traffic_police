package com.rjapps.mpf.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;

public class InvoiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());

        setButtonAction();
    }

    private void setButtonAction() {
        if ("issue_fine".equals(getIntent().getStringExtra("type"))) {
            Button addFineButton = findViewById(R.id.add_more_fine);
            addFineButton.setVisibility(View.VISIBLE);
            addFineButton.setOnClickListener(view -> {
                finish();
            });
        }

        findViewById(R.id.confirm_invoice).setOnClickListener(view -> showFineDetailDialog());
        findViewById(R.id.detail).setOnClickListener(view -> showDetailDialog());
    }

    private void showDetailDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.fine_detail_dialog_layout, null);
        dialogBuilder.setView(dialogView);

        AlertDialog alertDialog = dialogBuilder.create();
        dialogView.findViewById(R.id.close_dialog).setOnClickListener(view -> alertDialog.dismiss());

        alertDialog.show();
    }

    void showFineDetailDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.create_fine_detail_dialog_layout, null);
        dialogBuilder.setView(dialogView);

        AlertDialog alertDialog = dialogBuilder.create();
        dialogView.findViewById(R.id.close_dialog).setOnClickListener(view -> alertDialog.dismiss());

        alertDialog.show();
    }
}