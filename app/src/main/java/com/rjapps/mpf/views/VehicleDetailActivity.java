package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;

public class VehicleDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_detail);
        setButtonAction();
    }

    private void setButtonAction() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());

        findViewById(R.id.routine_check).setOnClickListener(view -> startActivity(new Intent(VehicleDetailActivity.this, RoutineCheckActivity.class)));

        findViewById(R.id.issue_fine).setOnClickListener(view -> startActivity(new Intent(VehicleDetailActivity.this, CreateFineActivity.class)));
    }
}