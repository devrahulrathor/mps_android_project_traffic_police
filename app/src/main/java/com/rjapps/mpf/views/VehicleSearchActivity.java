package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;

public class VehicleSearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_search);

        setButtonAction();
    }

    private void setButtonAction() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());
        findViewById(R.id.search_id).setOnClickListener(view -> startActivity(
                new Intent(VehicleSearchActivity.this,
                        VehicleDetailActivity.class)));
    }
}