package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;
import com.rjapps.mpf.views.adapter.SpinnerAdapter;

public class RoutineCheckActivity extends AppCompatActivity {
    private Spinner statusSpinner;
    private Spinner issueSpinner;
    private Button confirmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routine_check);
        init();
        setButtonAction();
        setSpinner();
    }

    void init() {
        statusSpinner = findViewById(R.id.status_spinner);
        issueSpinner = findViewById(R.id.issue_spinner);
        confirmButton = findViewById(R.id.confirm_button);
    }

    void setSpinner() {
        SpinnerAdapter statusAdapter = new SpinnerAdapter(RoutineCheckActivity.this, android.R.layout.simple_list_item_1);
        statusAdapter.clear();
        statusAdapter.addAll(getResources().getStringArray(R.array.status_array));
        statusAdapter.add(getString(R.string.status));
        statusSpinner.setAdapter(statusAdapter);
        statusSpinner.setSelection(statusAdapter.getCount());
        statusSpinnerColor();

    }

    private void statusSpinnerColor() {
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String status = statusSpinner.getSelectedItem().toString();
//                    if ("Status".equals(status)) {
//                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_grey));
//                    } else {
//                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
//                    }
                    if ("All Good, Checked".equals(status)) {
                        showDetailDialog();
                    } else if ("Not good, Issue Fine".equals(status)) {
                        findViewById(R.id.issue_spinner_layout).setVisibility(View.VISIBLE);
                        setIssueSpinner();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void showDetailDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.status_dialog_layout, null);
        dialogBuilder.setView(dialogView);

        AlertDialog alertDialog = dialogBuilder.create();
        dialogView.findViewById(R.id.go_back_button).setOnClickListener(view -> {
            //////
            alertDialog.dismiss();
            finish();
        });

        alertDialog.show();
    }

    private void setIssueSpinner() {
        SpinnerAdapter issueAdapter = new SpinnerAdapter(RoutineCheckActivity.this, android.R.layout.simple_list_item_1);
        issueAdapter.clear();
        issueAdapter.addAll(getResources().getStringArray(R.array.issue_array));
        issueAdapter.add(getString(R.string.select_the_issues));
        issueSpinner.setAdapter(issueAdapter);
        issueSpinner.setSelection(issueAdapter.getCount());

        issueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String status = issueSpinner.getSelectedItem().toString();
//                if ("Select the issues".equals(status)) {
//                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_grey));
//                } else {
//                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
//                }
                confirmButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setButtonAction() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());
        confirmButton.setOnClickListener(view -> {
            ///
            startActivity(new Intent(RoutineCheckActivity.this, InvoiceActivity.class));
        });

    }
}