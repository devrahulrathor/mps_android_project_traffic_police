package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;
import com.rjapps.mpf.views.adapter.SpinnerAdapter;

public class CreateFineActivity extends AppCompatActivity {
    private Spinner fineTypeSpinner;
    private Spinner subTypeSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_fine);
        init();
        setButtonAction();
        setSpinner();
    }

    void init() {
        fineTypeSpinner = findViewById(R.id.fine_type_spinner);
        subTypeSpinner = findViewById(R.id.sub_type_spinner);
    }

    private void setSpinner() {
        SpinnerAdapter fineTypeAdapter = new SpinnerAdapter(CreateFineActivity.this, android.R.layout.simple_list_item_1);
        fineTypeAdapter.clear();
        fineTypeAdapter.addAll(getResources().getStringArray(R.array.id_type));
        fineTypeAdapter.add(getString(R.string.select_fine_type));
        fineTypeSpinner.setAdapter(fineTypeAdapter);
        fineTypeSpinner.setSelection(fineTypeAdapter.getCount());
        fineTypeSpinnerColor();

        SpinnerAdapter subTypeAdapter = new SpinnerAdapter(CreateFineActivity.this, android.R.layout.simple_list_item_1);
        subTypeAdapter.clear();
        subTypeAdapter.addAll(getResources().getStringArray(R.array.id_type));
        subTypeAdapter.add(getString(R.string.select_a_subtype));
        subTypeSpinner.setAdapter(subTypeAdapter);
        subTypeSpinner.setSelection(subTypeAdapter.getCount());
        subTypeSpinnerColor();
    }

    private void fineTypeSpinnerColor() {
        fineTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String group = fineTypeSpinner.getSelectedItem().toString();
//                if ("Select Fine Type".equals(group)) {
//                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_grey));
//                } else {
//                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void subTypeSpinnerColor() {
        subTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String group = subTypeSpinner.getSelectedItem().toString();
//                if ("Select a Subtype".equals(group)) {
//                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_grey));
//                } else {
//                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setButtonAction() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());
        findViewById(R.id.confirm_button).setOnClickListener(view -> {
            Intent intent = new Intent(CreateFineActivity.this, InvoiceActivity.class);
            intent.putExtra("type", "issue_fine");
            startActivity(intent);
        });
    }
}