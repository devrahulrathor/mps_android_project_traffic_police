package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;
import com.rjapps.mpf.util.MyApplication;

public class SplashActivity extends AppCompatActivity {
    private static final int SPLASH_DURATION = 5000;
    private MyApplication myApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        myApplication = MyApplication.getInstance();
        splashScreen();
    }

    private void splashScreen() {
        new Handler().postDelayed(() -> {
            goTOActivity();
        }, SPLASH_DURATION);
    }

    public void goTOActivity() {
        if (myApplication.getIsLogin()) {
            Log.e("TOKEN", "TOKEN: "+myApplication.getToken() );
            Log.e("RTOKEN", "RTOKEN: "+myApplication.getRefreshToken() );
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        }

    }
}