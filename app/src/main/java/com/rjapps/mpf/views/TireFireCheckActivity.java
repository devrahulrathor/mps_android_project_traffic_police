package com.rjapps.mpf.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;

public class TireFireCheckActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tire_check);

        if ("Fire".equals(getIntent().getStringExtra("type"))) {
            findViewById(R.id.fire_container).setVisibility(View.VISIBLE);
            setFireContainer();
        } else {
            findViewById(R.id.tire_container).setVisibility(View.VISIBLE);
            setTireContainer();
        }
        setButtonAction();
    }

    private void setTireContainer() {


        textView = findViewById(R.id.fines_page);
        SpannableString text = new SpannableString("The check seems alright, though a fine for low tube air is added on the Fines Page");
        text.setSpan(new ForegroundColorSpan(Color.BLUE), 72, 82, 0);
        final Context context = this;

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Fines Page", Toast.LENGTH_LONG).show();
            }
        };
        text.setSpan(clickableSpan, 72, 82, 0);

        textView.setText(text, TextView.BufferType.SPANNABLE);

    }

    private void setFireContainer() {

    }

    private void setButtonAction() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());

        findViewById(R.id.go_back).setOnClickListener(view -> finish());
        findViewById(R.id.check_fine).setOnClickListener(view -> {
            Intent intent = new Intent(TireFireCheckActivity.this, InvoiceActivity.class);
            intent.putExtra("type", "license_search");
            startActivity(intent);
        });
    }
}