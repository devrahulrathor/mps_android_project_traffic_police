package com.rjapps.mpf.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.rjapps.mpf.R;

public class FireExtinguisherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fire_extinguisher);
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());

    }
}