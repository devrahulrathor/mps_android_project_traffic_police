package com.rjapps.mpf.views;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rjapps.mpf.R;
import com.rjapps.mpf.util.LicenceCallBack;
import com.rjapps.mpf.util.MyApplication;
import com.rjapps.mpf.viewmodel.AccidentViewModel;
import com.rjapps.mpf.views.adapter.ImagesAdapter;
import com.rjapps.mpf.views.adapter.LicenseAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class ReportAccidentActivity extends AppCompatActivity {

    private LinearLayout licenseLayout;
    private EditText accidentDescription;
    private EditText firstLicense;
    private RecyclerView licenseRecycler;
    private RecyclerView imagesRecycler;
    final int SELECT_PICTURE_CODE = 101;
    private static final int REQUEST_LOCATION = 103;
    private List<File> imagesList;
    private List<String> licenceList;
    private LicenseAdapter licenseAdapter;
    private ImagesAdapter imagesAdapter;
    private static final int CAMERA_PERMISSION_CODE = 100;
    private LocationManager locationManager;
    private String latitude, longitude;
    private AccidentViewModel accidentViewModel;
    private MyApplication myApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_accident);
        getLocation();
        init();
        setButtonAction();
        setGetDataViewModel();
    }

    private void setGetDataViewModel() {
        accidentViewModel.reportedSuccess.observe(this, s -> {
            Toast
                    .makeText(ReportAccidentActivity.this, "" + s, Toast.LENGTH_SHORT)
                    .show();
            finish();
        });

        accidentViewModel.reportFailed.observe(this, s -> Toast
                .makeText(ReportAccidentActivity.this, "" + s, Toast.LENGTH_SHORT)
                .show());
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        getLocation();
//    }

    private void getLocation() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            OnGPS();
        } else {
            getCurrentLocation();
        }
    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false)
                .setPositiveButton("Yes", (dialog, which) -> startActivity(
                        new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton("No", (dialog, which) -> dialog.cancel());
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(
                ReportAccidentActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                ReportAccidentActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
                Log.e("ACCIDENT", "getLocation: " + latitude + " & " + longitude);
            } else {
//                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void init() {
        licenseLayout = findViewById(R.id.license_layout);
        accidentDescription = findViewById(R.id.accident_description);
        firstLicense = findViewById(R.id.first_license);
        licenseRecycler = findViewById(R.id.license_recycler);
        imagesRecycler = findViewById(R.id.photos_recycler);
        licenceList = new ArrayList<>();
        imagesList = new ArrayList<>();
        accidentViewModel = new ViewModelProvider(this).get(AccidentViewModel.class);
        myApplication = MyApplication.getInstance();
        setLicenseAdapter(licenceList);
        setImagesAdapter(imagesList);
        licenceDeleteListener();
        imageDeleteListener();
    }


    private void licenceDeleteListener() {
        licenseAdapter.setOnItemClickListener(new LicenceCallBack() {
            @Override
            public void onItemClick(int position) {
            }

            @Override
            public void deleteItem(int position) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ReportAccidentActivity.this)
                        .setTitle("Delete License!")
                        .setMessage("Are you sure to delete this License?")
                        .setCancelable(false)
                        .setNegativeButton("No", (dialog, which) -> dialog.cancel())
                        .setPositiveButton("Yes", (dialog, which) -> {
                            licenceList.remove(position);
                            licenseAdapter.notifyDataSetChanged();
                            dialog.cancel();
                        });

                AlertDialog alert = alertDialog.create();
                alert.show();
            }
        });
    }

    private void imageDeleteListener() {
        imagesAdapter.setOnItemClickListener(new LicenceCallBack() {
            @Override
            public void onItemClick(int position) {
            }

            @Override
            public void deleteItem(int position) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ReportAccidentActivity.this)
                        .setTitle("Delete License!")
                        .setMessage("Are you sure to delete this License?")
                        .setCancelable(false)
                        .setNegativeButton("No", (dialog, which) -> dialog.cancel())
                        .setPositiveButton("Yes", (dialog, which) -> {
                            imagesList.remove(position);
                            imagesAdapter.notifyDataSetChanged();
                            dialog.cancel();
                        });

                AlertDialog alert = alertDialog.create();
                alert.show();
            }
        });
    }


    private void setButtonAction() {
        findViewById(R.id.pick_images).setOnClickListener(view -> {
            checkPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
        });
        findViewById(R.id.report_accident).setOnClickListener(view -> {
            ///TODO: Report accident
            reportAccident();
        });

        findViewById(R.id.back_icon).setOnClickListener(view -> finish());

        findViewById(R.id.add_driver_license).setOnClickListener(view -> {
            String firstLicenseString = firstLicense.getText().toString().trim();
            if ("".equals(firstLicenseString)) {
                Toast.makeText(ReportAccidentActivity.this, "Please Enter Licence Number!", Toast.LENGTH_SHORT).show();
            } else {
                licenceList.add(firstLicenseString);
                firstLicense.setText("");
                licenseAdapter.notifyDataSetChanged();
            }
        });
    }

    private void reportAccident() {
        String descriptionString = accidentDescription.getText().toString().trim();
        latitude = "80.223423";
        longitude = "30.9879";
        if (licenceList.isEmpty()) {
            Toast.makeText(ReportAccidentActivity.this, "Please enter at least ont driving license", Toast.LENGTH_SHORT).show();
        } else if (imagesList.isEmpty()) {
            Toast.makeText(ReportAccidentActivity.this, "Please capture at least one image", Toast.LENGTH_SHORT).show();
        } else if ("".equals(descriptionString)) {
            Toast.makeText(ReportAccidentActivity.this, "Please enter description", Toast.LENGTH_SHORT).show();
        } else {
            Log.e("ACCIDENT", "reportAccident: " + myApplication.getToken());
            accidentViewModel.reportAnAccident(myApplication.getToken(), imagesList, licenceList, descriptionString, latitude, longitude);
        }
    }

    void setLicenseAdapter(List<String> list) {
        licenseRecycler.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        licenseAdapter = new LicenseAdapter(ReportAccidentActivity.this, list);
        licenseRecycler.setAdapter(licenseAdapter);
    }

    void setImagesAdapter(List<File> list) {
        imagesRecycler.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        imagesAdapter = new ImagesAdapter(ReportAccidentActivity.this, list);
        imagesRecycler.setAdapter(imagesAdapter);
    }

    public void checkPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(ReportAccidentActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(ReportAccidentActivity.this, new String[]{permission}, requestCode);
        } else {
            takeCameraImage();
        }
    }

    void takeCameraImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, SELECT_PICTURE_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,
                permissions,
                grantResults);

        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takeCameraImage();
            } else {
                Toast.makeText(ReportAccidentActivity.this, "Camera Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PICTURE_CODE && resultCode == RESULT_OK && data != null) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            saveFile(photo);
        }
    }

    void saveFile(Bitmap bitmap) {
        try {
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();

            File f = new File(getCacheDir(), ts + ".png");
            f.createNewFile();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            if (imagesList.size() < 10) {
                imagesList.add(f);
                imagesAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(ReportAccidentActivity.this, "Only 10 images are allowed", Toast.LENGTH_SHORT).show();
            }
            Log.e("ACCIDENT", "FILE LENGTH = " + imagesList.size());
        } catch (Exception e) {
            Log.e("ACCIDENT", "saveFile: " + e.getMessage());
        }
    }

}