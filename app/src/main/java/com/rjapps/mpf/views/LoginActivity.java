package com.rjapps.mpf.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.rjapps.mpf.R;
import com.rjapps.mpf.util.MyApplication;
import com.rjapps.mpf.viewmodel.AuthViewModel;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    EditText userName;
    EditText password;
    AuthViewModel authViewModel;
    ProgressDialog progressDialog;
    MyApplication myApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        logIn();
        getViewModelData();
    }

    void init() {
        myApplication = MyApplication.getInstance();
        userName = findViewById(R.id.userName);
        password = findViewById(R.id.password);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        authViewModel = new ViewModelProvider(this).get(AuthViewModel.class);
    }

    private void getViewModelData() {
        authViewModel.loginSuccess.observe(this, value -> {
            progressDialog.dismiss();
            myApplication.saveIsLogin(true);
            myApplication.saveToken(value.getToken(), value.getRefreshToken());
            Log.e("LOGIN", "getViewModelData: " + value.getToken());
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        });

        authViewModel.loginFailed.observe(this, error -> {
            progressDialog.dismiss();
            Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_SHORT).show();
            Log.e("LOGIN_ERROR", "getViewModelData: error " + error);
        });
    }

    private void logIn() {
        findViewById(R.id.loginButton).setOnClickListener(view -> {
            progressDialog.show();
            String email = userName.getText().toString().trim();
            String pass = password.getText().toString().trim();

            HashMap<String, String> requestParam = new HashMap<>();
            requestParam.put("email", email);
            requestParam.put("password", pass);
//            requestParam.put("imei", "4901542032375180");
            requestParam.put("imei", "863794041811153");
            authViewModel.logIn(requestParam);
        });
    }
}