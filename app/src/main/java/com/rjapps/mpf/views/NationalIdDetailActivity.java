package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;

public class NationalIdDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_national_id_detail);

        setButtonActions();
    }

    private void setButtonActions() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());

        findViewById(R.id.routine_check).setOnClickListener(view -> {
            startActivity(new Intent(NationalIdDetailActivity.this, RoutineCheckActivity.class));
        });
        findViewById(R.id.issue_fine).setOnClickListener(view -> {
            ////
            startActivity(new Intent(NationalIdDetailActivity.this, CreateFineActivity.class));
        });
    }
}