package com.rjapps.mpf.views.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rjapps.mpf.R;
import com.rjapps.mpf.util.LicenceCallBack;

import java.io.File;
import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {
    private LicenceCallBack clickListener;
    private final Context context;
    private final List<File> list;

    public ImagesAdapter(Context context, List<File> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_images_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.image.setImageBitmap(getBitmap(list.get(position)));
        holder.deleteImage.setOnClickListener(view -> clickListener.deleteItem(position));
    }

    public void setOnItemClickListener(LicenceCallBack clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    Bitmap getBitmap(File file) {
        return BitmapFactory.decodeFile(file.getAbsolutePath());

    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ImageView deleteImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            deleteImage = itemView.findViewById(R.id.image_delete);
        }
    }
}
