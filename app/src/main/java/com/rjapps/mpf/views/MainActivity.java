package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.rjapps.mpf.R;

public class MainActivity extends AppCompatActivity {
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setNavDrawer();
        setButtonActions();

    }

    private void setButtonActions() {
        findViewById(R.id.license_search).setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, LicenseSearchActivity.class));
        });
        findViewById(R.id.vehicle_search).setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, VehicleSearchActivity.class);
            intent.putExtra("type", "vehicle_search");
            startActivity(intent);
        });
        findViewById(R.id.issue_fine_button).setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, IssueFineActivity.class);
            intent.putExtra("type", "issue_fine");
            startActivity(intent);
        });
        findViewById(R.id.report_an_accident).setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, ReportAccidentActivity.class));
        });

    }

    private void setNavDrawer() {
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.navBar);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.license_search) {
                    startActivity(new Intent(MainActivity.this, LicenseSearchActivity.class));
                }
//                else if (id == R.id.national_id_search) {
//                    Intent intent = new Intent(MainActivity.this, ScanActivity.class);
//                    intent.putExtra("type", "national_id_search");
//                    startActivity(intent);
//                }
                else if (id == R.id.vehicle_search) {
                    Intent intent = new Intent(MainActivity.this, VehicleSearchActivity.class);
                    intent.putExtra("type", "vehicle_search");
                    startActivity(intent);
                } else if (id == R.id.fine_issue) {
                    Intent intent = new Intent(MainActivity.this, IssueFineActivity.class);
                    intent.putExtra("type", "issue_fine");
                    startActivity(intent);
                } else if (id == R.id.report_an_accident) {
                    startActivity(new Intent(MainActivity.this, ReportAccidentActivity.class));
                } else if (id == R.id.logout) {
                    ///
                    Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                }
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        findViewById(R.id.drawer_icon).setOnClickListener(view -> {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}