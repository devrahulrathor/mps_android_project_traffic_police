package com.rjapps.mpf.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.rjapps.mpf.R;

public class LicenseSearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license_search);

        setButtonAction();
    }

    private void setButtonAction() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());

        findViewById(R.id.enter_manually).setOnClickListener(view -> {
            startActivity(new Intent(LicenseSearchActivity.this, LicenseDetailActivity.class));
        });
    }
}