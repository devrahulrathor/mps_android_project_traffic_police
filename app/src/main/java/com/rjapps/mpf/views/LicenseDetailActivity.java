package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;

public class LicenseDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license_detail);

        setButtonActions();
    }

    private void setButtonActions() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());
        if ("issue_fine".equals(getIntent().getStringExtra("type"))) {
            findViewById(R.id.routine_check).setVisibility(View.GONE);
        } else {
            findViewById(R.id.routine_check).setOnClickListener(view -> {
                startActivity(new Intent(LicenseDetailActivity.this, RoutineCheckActivity.class));
            });
        }

        findViewById(R.id.issue_fine).setOnClickListener(view -> startActivity(new Intent(LicenseDetailActivity.this, CreateFineActivity.class)));


    }
}