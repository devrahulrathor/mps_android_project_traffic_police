package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;

public class ScanActivity extends AppCompatActivity {

    Button scanButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        setButtonActions();
    }

    private void setButtonActions() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());

        scanButton = findViewById(R.id.scan_qr);
        if ("vehicle_search".equals(getIntent().getStringExtra("type"))) {
            scanButton.setText("Scan RC");
        }
        if ("issue_fine".equals(getIntent().getStringExtra("type"))) {
            scanButton.setText("Scan NIC/Plate Number");
            findViewById(R.id.or_upload_file).setVisibility(View.VISIBLE);
            findViewById(R.id.upload_file).setVisibility(View.VISIBLE);
            findViewById(R.id.upload_file).setOnClickListener(view -> {
                ///TODO Upload fine
            });
        }

        scanButton.setOnClickListener(view -> {
            //TODO: Write scan code here
            Toast.makeText(ScanActivity.this, "Scan QR", Toast.LENGTH_SHORT).show();
        });

        findViewById(R.id.enter_manually).setOnClickListener(view -> {
            if ("national_id_search".equals(getIntent().getStringExtra("type"))) {
                startActivity(new Intent(ScanActivity.this, NationalIdSearchActivity.class));

            } else if ("vehicle_search".equals(getIntent().getStringExtra("type"))) {
                startActivity(new Intent(ScanActivity.this, VehicleSearchActivity.class));
            } else if ("issue_fine".equals(getIntent().getStringExtra("type"))) {
                startActivity(new Intent(ScanActivity.this, IssueFineActivity.class));
            } else {
                startActivity(new Intent(ScanActivity.this, LicenseSearchActivity.class));
            }
        });
    }
}