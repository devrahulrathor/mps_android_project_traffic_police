package com.rjapps.mpf.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.rjapps.mpf.R;

public class NationalIdSearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_national_id_search);

        setButtonAction();
    }

    private void setButtonAction() {
        findViewById(R.id.back_icon).setOnClickListener(view -> finish());
        findViewById(R.id.search_id).setOnClickListener(view -> startActivity(new Intent(NationalIdSearchActivity.this, NationalIdDetailActivity.class)));
    }
}