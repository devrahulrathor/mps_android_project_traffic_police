package com.rjapps.mpf.data.repo.accident;

import android.util.Log;

import com.rjapps.mpf.data.network.retrofit.RetrofitObj;
import com.rjapps.mpf.data.repo.accident.callback.AccidentCallBack;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccidentRepo {
    public void reportAccident(String token, List<File> images, List<String> licenseNumbers, String description, String latitude, String longitude, AccidentCallBack callBack) {
        Log.e("TOKEN", "onResponse: TOKEN" + token);


        Map<String, RequestBody> map = new HashMap<>();

        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody lat = RequestBody.create(MediaType.parse("text/plain"), latitude);
        RequestBody longi = RequestBody.create(MediaType.parse("text/plain"), longitude);
        RequestBody licenses = RequestBody.create(MediaType.parse("text/plain"), licenseNumbers.toString());

        map.put("description", desc);
        map.put("Latitude", lat);
        map.put("Longitude", longi);
        map.put("licenceNos", licenses);

        for (int i = 0; i < images.size(); i++) {
            RequestBody file = RequestBody.create(MediaType.parse("image/*"), images.get(i));
            map.put("file" + i + 1, file);
        }

        RetrofitObj.getRequestInterfaceApi().reportAccident(token, map)
                .enqueue(new Callback<JSONObject>() {
                    @Override
                    public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                        try {
                            Log.e("ERROR", "onResponse: BODY" + response.body());
                            Log.e("ERROR", "onResponse: MESSAGE" + response.isSuccessful());
                            if (response.isSuccessful()) {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                if (jsonObject.getBoolean("status")) {
                                    callBack.onSuccess(jsonObject.getString("message"));
                                } else {
                                    callBack.onError(jsonObject.getString("message"));
                                }
                            } else {
//                                Log.e("ERROR", "onResponse: BODY" + response.errorBody().string());
//                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                callBack.onError("Please try again!");
//                                Log.e("ERROR", "onResponse: BODY" + jObjError.getString("message"));
                            }

                        } catch (Exception e) {
                            callBack.onError("Please try again! " + e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<JSONObject> call, Throwable t) {
                        callBack.onError("Please try again!" + t.getMessage());
                    }
                });
    }
}
