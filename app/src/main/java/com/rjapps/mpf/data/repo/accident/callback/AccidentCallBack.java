package com.rjapps.mpf.data.repo.accident.callback;


public interface AccidentCallBack {
    void onSuccess(String success);

    void onError(String error);
}
