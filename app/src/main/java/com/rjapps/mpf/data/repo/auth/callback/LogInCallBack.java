package com.rjapps.mpf.data.repo.auth.callback;

import com.rjapps.mpf.data.repo.auth.model.LogIn;

public interface LogInCallBack {
    void onSuccess(LogIn logIn);
    void onError(String error);
}
