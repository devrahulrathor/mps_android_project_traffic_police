package com.rjapps.mpf.data.repo.auth;

import android.util.Log;

import com.rjapps.mpf.data.network.retrofit.RetrofitObj;
import com.rjapps.mpf.data.repo.auth.callback.LogInCallBack;
import com.rjapps.mpf.data.repo.auth.model.LogInData;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthRepo {
    public void login(HashMap<String, String> data, LogInCallBack callBack) {
        RetrofitObj.getRequestInterfaceApi().logIn(data)
                .enqueue(new Callback<LogInData>() {
                    @Override
                    public void onResponse(Call<LogInData> call, Response<LogInData> response) {
                        try {
                            Log.e("ERROR", "onResponse: BODY" + response.body());
                            Log.e("ERROR", "onResponse: MESSAGE" + response.isSuccessful());
                            if (response.isSuccessful()) {
                                LogInData logInData = response.body();
                                assert logInData != null;
                                if (logInData.getStatus()) {
                                    callBack.onSuccess(logInData.getData());
                                } else {
                                    callBack.onError(logInData.getMessage());
                                }
                            } else {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                callBack.onError(jObjError.getString("message"));
                                Log.e("ERROR", "onResponse: BODY" + jObjError.getString("message"));
                            }

                        } catch (Exception e) {
                            callBack.onError("Please try again! " + e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<LogInData> call, Throwable t) {
                        callBack.onError("Please try again!" + t.getMessage());
                    }
                });
    }
}
